<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodPairing extends Model
{
    protected $table = 'food_pairings';
    public $timestamps = true;
    protected $guarded = [''];

    // protected $with = ['type'];

    // Relatie met drinks (buiten de scope)
    public function drink()
    {
        return $this->belongsTo('App\Drink');
    }

    // Relatie met foods (buiten de scope)
    public function foods()
    {
        return $this->hasOne('App\Food');
    }
}

<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;

class TypeController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('type.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    // Maak een nieuw Type object aan
    $type = new Type;
    // Vul het Type object met de waarde uit het request
    $type->name = $request->name;

    // Try-catch blok voor het afhandelen van eventuele errors in de save() actie
    try {
        // Sla het type op met bovenstaande waardes
        $type->save();
    } catch (\Exception $e) {
        // Algemene error message wanneer save niet slaagt
        return redirect()->back()->with('error', 'Het type is niet correct ingevoerd, probeer het aub nogmaals.');
    }
    // Als het type opgeslagen is, return met success message
    return redirect('/admin')->with('success', 'Het type '. $type->name .' is successvol toegevoegd!');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit()
  {
    // Haal alle Types op met ID en naam
    $types = Type::all(['id', 'name']);

    // Return de type edit view met bovenstaande variabele
    return view('type.edit', compact('types',$types));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request)
    {
        // Haal het Type op aan de hand van het ID in de request
        $type = Type::where('id','=',$request->type)->first();
        if($request->name){
            // Als er een nieuwe naam is ingevoerd, vul het opgevraagde type met de nieuwe naam
            $type->name = $request->name;
        }

        // Try-catch blok voor het afhandelen van eventuele errors in de save() actie
        try {
            // Sla het type op met bovenstaande waardes
            $type->save();
        } catch (\Exception $e) {
            // Algemene error message wanneer save niet slaagt
            return redirect()->back()->with('error', 'Het type kon niet aangepast worden, probeer het aub nogmaals.');
        }
        // Als het type opgeslagen is, return met success message
        return redirect('/admin')->with('success', 'Het type is succesvol aangepast!');
    }

    public function select()
    {
        // Haal alle types op
        $types = Type::all();
        // Return de view met types
        return view('type.select', compact('types',$types));
    }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy(Request $request)
  {
      // Vind het type om te verwijderen aan de hand van ID
      $type = Type::where('id',$request->type)->firstOrFail();

      // Try-catch blok voor het afhandelen van eventuele errors in de delete() actie
      try {
          // Verwijder het geselecteerde type
          $type->delete();
      } catch (\Exception $e) {
          // Algemene error message wanneer delete niet slaagt
          return redirect()->back()->with('error', 'Het type kon niet verwijderd worden, probeer het aub nogmaals.');
      }

      // Als het type verwijderd is, return met success message
      return redirect(route('admin'))->with('success', 'Het type is succesvol verwijderd.');
  }

}

?>

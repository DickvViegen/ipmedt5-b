<?php

namespace App\Http\Controllers;

use App\Drink;
use App\Type;
use App\View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;



class DrinkController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

    // Haal alle dranken in de database op en pak daar de eerste 5 van
	$popularDrinks = Drink::all()->take(5);

    // Return deze objecten
	return $popularDrinks;
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    // Haal alle Types op voor de selectbox in de view
    $types = Type::all();

    // Return de view met types
    return view('drink.create')->with('types',$types);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    // Haal het type op uit de selectbox, parse de naam naar een ID
    $type_id = Type::where('name','=',$request->type)->first()->id;

    // Maak een nieuw Drink object aan
    $drink = new Drink;

    // Vul het drink object met de request waardes
    $drink->name = $request->name;
    $drink->brand = $request->brand;
    $drink->country = $request->country;
    $drink->type_id = $type_id;
    $drink->region = $request->region;
    $drink->age = $request->age;
    $drink->price = $request->price;
    $drink->discount = $request->discount ? $request->discount : false;
    $drink->discount_price = $request->discount_price;
    $drink->taste = $request->taste;
    $drink->smokiness = $request->smokiness;
    $drink->distillery = $request->distillery ? $request->distillery : null;
    $drink->barcode = $request->barcode ? $request->barcode : null;
    $drink->description = $request->description;
    $drink->description_short = $request->description_short;
    $drink->cask = $request->cask;

    // Try-catch blok voor het afhandelen van eventuele errors in de save() actie
    try {
        // Sla de drank op met bovenstaande waardes
        $drink->save();
    } catch (\Exception $e) {
        // Algemene error message wanneer save niet slaagt
        return redirect()->back()->with('error', 'De ingevoerde data was niet compleet, probeer het aub nog een keer.');
    }
    // Als de drank opgeslagen is, return met success message
    return redirect('/admin')->with('success', 'De drank '. $drink->name .' is succesvol ingevoerd!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    // Vind een enkele drank dmv ID
    $drink = Drink::find($id);

    return $drink;
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit(Request $request)
  {
    // Haal alle types op voor de selectbox
    $types = Type::all();

    // Vind de drank die gekozen is aan de hand van ID
    $drink = Drink::where('id','=',$request->drink)->firstOrFail();

    // Geef het type van de drank mee aan de view om de selectbox te prefillen
    $selectedType = $drink->type->name;

    // Return view met bovenstaande variabelen
    return view('drink.edit')->with('drink',$drink)->with('types',$types)->with('selectedType',$selectedType);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request)
  {
     // Vind het ingevoerde type en parse het naar een ID
     $type_id = Type::where('name','=',$request->type)->firstOrFail()->id;

     // Vind de geselecteerde drank en vul de waardes met de waardes in het request
     $drink = Drink::where('id',$request->id)->firstOrFail();
     $drink->name = $request->name;
     $drink->brand = $request->brand;
     $drink->country = $request->country;
     $drink->type_id = $type_id;
     $drink->region = $request->region;
     $drink->age = $request->age;
     $drink->price = $request->price;
     $drink->discount = $request->discount ? $request->discount : false;
     $drink->discount_price = $request->discount_price;
     $drink->taste = $request->taste;
     $drink->smokiness = $request->smokiness;
     $drink->distillery = $request->distillery ? $request->distillery : null;
     $drink->barcode = $request->barcode ? $request->barcode : null;
     $drink->description = $request->description;
     $drink->description_short = $request->description_short;
     $drink->cask = $request->cask;

     // Try-catch blok voor het afhandelen van eventuele errors in de save() actie
     try {
         // Sla de drank op met bovenstaande waardes
         $drink->save();
     } catch (\Exception $e) {
         // Algemene error message wanneer save niet slaagt
         return redirect()->back()->with('error', 'De ingevoerde velden waren niet allemaal correct ingevuld, probeer het aub nogmaals.');
     }
     // Als de drank opgeslagen is, return met success message
     return redirect('/admin')->with('success', 'De drank '. $drink->name .' is succesvol aangepast!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy(Request $request)
  {
      // Vind de drank om te verwijderen aan de hand van ID
      $drink = Drink::where('id',$request->drink)->firstOrFail();

      // Try-catch blok voor het afhandelen van eventuele errors in de delete() actie
      try {
          // Verwijder de geselecteerde drank
          $drink->delete();
      } catch (\Exception $e) {
          // Algemene error message wanneer delete niet slaagt
          return redirect()->back()->with('error', 'De geselecteerde drank kon niet verwijderd worden, probeer het nog eens.');
      }
      // Als de drank verwijderd is, return met success message
      return redirect(route('admin'))->with('success', 'De drank '. $drink->name .' is succesvol verwijderd!');
  }

  public function select()
  {
    // Genereer de waardes voor de selectbox op de selectiepagina
    $drinks = Drink::all();

    // Return de select view
    return view('drink.select', compact('drinks',$drinks));
  }

  // Dit was een potentiële functie, het updaten van het aantal keer dat een drank was gezien, viel buiten scope
  public function updateViews(Request $request)
  {

    $view = new View;

    $view->drink_id = $request->drink_id;

    $view->save();

  }

  // Genereer view om drank te selecteren
  public function write()
  {
    // Selecteer alle dranken
    $drinks = Drink::all();

    // Return view met dranken
    return view('drink.write')->with('drinks', $drinks);
  }

  // Functie om een drank ID naar een NFC-tag te schrijven
  public function writeToRfid(Request $request)
  {
    // Haal het ID van de drank op uit de selectie
    $id = $request->id;

    // Maak een nieuw Symphony Process aan (roep een process aan vanuit de terminal) in dit geval een python script met het drank ID als argument
    $process = new Process('sudo python ' .storage_path(). '/scripts/whiskeywrite.py ' .$id);
    $process->run();

    // Check of process succesvol doorlopen is
    if(!$process->isSuccessful())
    {
      // Als het proces niet doorlopen is, stuur de gebruiker terug met een error message
      throw new ProcessFailedException($process);
      return redirect()->back()->with('error', 'Het beschrijven van de tag is niet gelukt, probeer het nog eens.');
    }

    // Als de tag beschreven is, stuur de gebruiker terug met een success message
    return redirect()->back()->with('success', 'De tag is succesvol beschreven met het ID: '.$id.'.');

  }

}

?>

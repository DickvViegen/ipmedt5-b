<?php 

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});


Route::resource('merk', 'merkController');
Route::resource('type', 'TypeController');
Route::resource('land', 'LandController');
Route::resource('streek', 'StreekController');
Route::resource('description', 'DescriptionController');
Route::resource('karakteristiek', 'KarakteristiekController');
Route::resource('beste_bij', 'Beste_bijController');
Route::resource('drank', 'DrankController');
Route::resource('drank_beste_bij', 'Drank_beste_bijController');
Route::resource('herkomst', 'HerkomstController');
Route::resource('drank_karakteristiek', 'Drank_karakteristiekController');

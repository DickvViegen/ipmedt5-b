<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drink extends Model {

		protected $table = 'drink';
		public $timestamps = true;
		protected $guarded = [''];

		protected $with = ['type'];

		// Leg de relatie met types
		public function type()
		{
			return $this->belongsTo('App\Type');
		}

		// Leg de relatie met Views (dit was een functie die buiten de scope is komen te vallen)
        public function views()
        {
            return $this->hasMany('App\View');
        }

		// Leg de relatie met Food Pairings (dit was een functie die buiten de scope is komen te vallen)
		public function foodPairing()
		{
			return $this->hasMany('App\FoodPairing');
		}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $table = 'foods';
    public $timestamps = true;
    protected $guarded = [''];

    // protected $with = ['type'];

    // Relatie met Food Pairings (buiten de scope)
    public function foodPairing()
    {
        return $this->belongsTo('App\FoodPairing');
    }

}

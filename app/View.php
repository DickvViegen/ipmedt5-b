<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $table = 'views';
    public $timestamps = true;
    protected $guarded = [''];

    // Leg de relatie met drinks (buiten de scope)
    public function drinks(){
        return $this->belongsTo('App\Drink');
    }
}

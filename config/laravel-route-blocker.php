<?php

    return [

        # WHITELIST EXAMPLE
        'whitelist' => [

            'localhost' => [
                '127.0.0.1',
                '127.0.0.2',
                '192.168.17.0',
                '10.0.0.*'
            ],
            'reactapp' => [
                '192.168.178.64',
                '192.168.178.56',
                '192.168.178.20',
            ],
        ],

        # RESPONSE SETTINGS
        'redirect_to'      => '/noaccess',   # URL TO REDIRECT IF BLOCKED (LEAVE BLANK TO THROW STATUS)
        'response_status'  => 403,  # STATUS CODE (403, 404 ...)
        'response_message' => ''    # MESSAGE (COMBINED WITH STATUS CODE)

    ];

?>

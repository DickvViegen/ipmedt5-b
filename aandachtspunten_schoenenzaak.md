# IPMEDT5 Groep B - Casus schoenenzaak
*Gerson Straver, Arthur van den Berg, Marius Stolp, Rick Jansen, Dick van Viegen*


## Welk vraagstuk koppel je aan je idee?
*Informatievoorziening klanten*:

Klanten kunnen de schoenen op een box/tafel/opzetje plaatsen, de schoen zal dan met bijbehorende informatie op een scherm worden getoond, denk hierbij aan;

    - welke maten nog op voorraad zijn,
    - wanneer bepaalde producten weer op voorraad zullen zijn,
    - in welke kleuren de schoen beschikbaar is,
    - welke schoenen eventueel vergelijkbaar zijn,
    - waar is de schoen gemaakt (fairtrade?)

*Efficiëntie medewerkers*:

Medewerkers zullen minder hoeven helpen en vooral minder naar achter hoeven lopen om de voorraad te checken, klanten hebben dit zelf al kunnen doen.

Klanten kunnen gerichter vragen stellen, de medewerker kan hier gerichter mee naar de opslag om schoenen te zoeken.

## Uitwerking
Voor deze casus zullen we gebruik maken van NFC ofwel RFID protocollen. De stickers worden onder ofwel de schoen of verpakking geplaatst, welke dan op een daarvoor ingerichte tafel geplaatst kan worden. Deze sticker word uitgelezen en er zal op het scherm naast deze tafel informatie over de schoen getoond worden.

Wanneer de klant in de buurt komt van de tafel, komt er een instructie op het scherm, die uitlegt hoe en waar het product te plaatsen.

Hiernaast willen we er voor zorgen dat wanneer de klant wegloopt van de tafel, het scherm weer op een standaard view gaat; de meest bekeken producten.

### Sensoren
    - RFID reader
    - Afstands sensor

## Software
    - Laravel
    - Firebase

## Impact vs. haalbaarheid
Impact: Wij denken dat dit een hoge impact zal hebben. Klanten kunnen zelf al redelijk achterhalen of schoenen nog op voorraad zijn, welke kleuren en welke maten beschikbaar zijn. Dit scheelt zowel de medewerker als de klant tijd. 

Haalbaarheid: Haalbaar is het wel, de techniek beheersen we al een beetje, de afstandssensor zal wel nog even uitgezocht moeten worden. Hoe we precies de communicatie met de backend doen, is nog even de vraag.


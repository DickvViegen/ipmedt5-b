# IPMEDT5 Groep B - Casus Slijter
*Gerson Straver, Arthur van den Berg, Marius Stolp, Rick Jansen, Dick van Viegen*


## Welk vraagstuk koppel je aan je idee?
*Informatievoorziening klanten* - Klanten kunnen zelf door middel van ons systeem bekijken waar de drank vandaan komt, hoe deze gemaakt is, bij welke gerechten deze zou passen.

*Informatievoorziening retailer* - Retailer zal kunnen zien welke dranken voldoende interesse wekken om informatie over op te vragen, hier valt dan weer de marketingstrategie op aan te passen.

*Workflow medewerkers* - medewerkers zullen over minder kennis hoeven te beschikken, het sollicitatie proces wordt hierdoor makkelijker. Medewerkers zullen ook meer tijd hebben voor andere klusjes binnen de zaak, want klanten hoeven het niet per se aan de medewerkers te vragen.


## Uitwerking
Hiervoor gaan we gebruik maken van het nfc ofwel rfid protocol, we plaatsen stickers onder de flessen/verpakkingen die, door de fles/verpakking op een bepaalde plek op een tafel te zetten, uitgelezen worden en de informatie zal op een scherm worden getoond.

Wanneer de klant in de buurt komt van de tafel, komt er een instructie op het scherm, die uitlegt hoe en waar het product te plaatsen.

Hiernaast willen we er voor zorgen dat wanneer de klant wegloopt van de tafel, het scherm weer op een standaard view gaat; de meest bekeken producten.

## Sensoren
    - RFID reader
    - Afstands sensor

## Software
    - Laravel
    - Firebase

## Impact vs. haalbaarheid
Impact: Het kan in theorie veel impact hebben - het schept een moderner beeld van de winkel, de klanten zullen sneller door het winkel proces heen lopen en medewerkers zullen meer tijd hebben voor andere werkzaamheden.

Haalbaarheid: Haalbaar is het wel, de techniek beheersen we al een beetje, de afstandssensor zal wel nog even uitgezocht moeten worden. Hoe we precies de communicatie met de backend doen, is nog even de vraag.


#!/bin/bash
mysql -uhomestead -psecret -e use homestead;
mysql -uhomestead -psecret -e SET FOREIGN_KEY_CHECKS = 0;
mysql -uhomestead -psecret -e SET @tables = NULL;
mysql -uhomestead -psecret -e SET GROUP_CONCAT_MAX_LEN=32768;
mysql -uhomestead -psecret -e SELECT GROUP_CONCAT('`',table_schema,'`.`',table_name, '`') INTO @tables FROM information_schema.tables WHERE table_schema = (SELECT DATABASE());
mysql -uhomestead -psecret -e SELECT IFNULL(@tables, '') INTO @tables;
mysql -uhomestead -psecret -e SET @tables = CONCAT('DROP TABLE IF EXISTS ',@tables);
mysql -uhomestead -psecret -e PREPARE stmt FROM @tables;
mysql -uhomestead -psecret -e EXECUTE stmt;
mysql -uhomestead -psecret -e DEALLOCATE PREPARE stmt;
mysql -uhomestead -psecret -e SET FOREIGN_KEY_CHECKS = 1;
@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="col-md-6">
		<div class="col-md-12">
			<h2>Dranken</h2>
		</div>
		<div class="col-md-12 button-container">
			<a class="btn btn-primary outline btn-lg btn-block" href="{{route('drink.create')}}">Drank invoeren</a>
		</div>
		<div class="col-md-12 button-container">
			<a class="btn btn-primary outline btn-lg btn-block" href="{{route('drink.select')}}">Drank aanpassen</a>
		</div>
		<div class="col-md-12 button-container">
			<a class="btn btn-danger outline btn-lg btn-block" href="{{route('drink.selectDestroy')}}">Drank verwijderen</a>
		</div>
		<div class="col-md-12 button-container">
			<a class="btn btn-primary outline btn-lg btn-block" href="{{route('drink.write')}}">Beschrijf tag</a>
		</div>

	</div>
	<div class="col-md-6">
		<div class="col-md-12">
			<h2>Types</h2>
		</div>
		<div class="col-md-12 button-container">
			<a class="btn btn-primary outline btn-lg btn-block" href="{{route('type.create')}}">Type invoeren </a>
		</div>
		<div class="col-md-12 button-container">
			<a class="btn btn-primary outline btn-lg btn-block" href="{{route('type.edit')}}">Type aanpassen</a>
		</div>
		<div class="col-md-12 button-container">
			<a class="btn btn-danger outline btn-lg btn-block" href="{{route('type.select')}}">Type verwijderen</a>
		</div>
	</div>
	<div class="col-md-12">
			<hr/>
	</div>
	<div class="col-md-6 col-md-offset-3 button-logout">
		<a href="{{route('logout')}}" class="btn btn-danger outline btn-lg btn-block">Log uit</a>
	</div>
</div>


@endsection

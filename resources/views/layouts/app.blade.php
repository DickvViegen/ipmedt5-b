<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>IoT IPMEDT5</title>
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>
        @include('layouts.header')
        <div class="main">
            @include('layouts.notification')
            @yield('content')
        </div>

        <!-- SCRIPTS -->
        <script type="text/javascript" href="/js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" href="/js/bootstrap.min.js"></script>
        <script type="text/javascript" href="/js/main.js"></script>
        <footer>
        </footer>
    </body>

</html>

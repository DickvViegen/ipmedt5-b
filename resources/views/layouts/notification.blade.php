@if(Session::has('error'))
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="alert alert-danger text-center">
                <strong>Foutmelding!</strong> {{Session::get('error')}}
            </div>
        </div>
    </div>
</div>
@endif

@if(Session::has('success'))
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="alert alert-success text-center">
                <strong>Success!</strong> {{Session::get('success')}}
            </div>
        </div>
    </div>
</div>
@endif

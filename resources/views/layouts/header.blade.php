<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src="{{url('/img/logo.png')}}" class="img-responsive center-block"/>
        </div>
    </div>
</div>

@extends('layouts.app')
@section('content')

{{Form::open(['route'=>['type.store'], 'class'=>'form-horizontal', 'method'=>'POST'])}}

<div class="container-fluid" style="padding-top:3em;">
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
			<label for="name"> Typenaam </label>
			<input class="form-control" type="text" id="name" name="name">
			<button class="btn btn-primary outline btn-block btn-lg" type="submit" style="margin-top: 2em;"> Invoeren </button>
			<a class="btn btn-warning outline btn-block last-button" style="margin-top: 1em;" href="{{route('admin')}}">Terug</a>
		</div>
	</div>
</div>
{{Form::close()}}
@endsection

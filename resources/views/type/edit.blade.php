@extends('layouts.app')
@section('content')

{{Form::open(['route'=>['type.update'], 'class'=>'form-horizontal', 'method'=>'UPDATE'])}}

<div class="container-fluid">
	<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-2">
		<h2> Kies een drank om aan te passen </h2>
		  {!! Form::Label('type', 'Type:') !!}
		  <select class="form-control" name="type">
		    @foreach($types as $type)
		      <option value="{{$type->id}}">{{$type->name}}</option>
		    @endforeach
		  </select>
		<label for="type">Voer het juiste type drank in</label>
		<input class="form-control" type="text" id="type" name="name">
		<button class="btn btn-primary outline btn-block btn-lg" type="submit" style="margin-top: 2em;"> Pas aan </button>
		<a class="btn btn-warning outline btn-block last-button" style="margin-top: 1em;" href="{{route('admin')}}">Terug</a>
	</div>
</div>
{{Form::close()}}
@endsection

@extends('layouts.app')
@section('content')

{{Form::open(['route'=>['type.destroy'], 'class'=>'form-horizontal', 'method'=>'POST'])}}
<div class="container-fluid">
	<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-2">
		<h2> Kies een drank om aan te passen </h2>
		<div class="form-group">
			{!! Form::Label('type', 'Type:') !!}
			<select class="form-control" name="type">
		    	@foreach($types as $type)
		    		<option value="{{$type->id}}">{{$type->name}}</option>
		    	@endforeach
		  	</select>
			<button type="submit" class="btn btn-danger outline btn-block btn-lg" style="margin-top: 2em;" name="destroy">Verwijder type</button>
			<button class="btn btn-warning outline btn-block" style="margin-top: 1em;" href="/admin">Terug</button>
		</div>
	</div>
</div>
{{Form::close()}}

@endsection

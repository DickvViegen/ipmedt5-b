@extends('layouts.app')
@section('content')

@if(Route::currentRouteName() == 'drink.selectDestroy')
{{Form::open(['route'=>['drink.destroy'], 'class'=>'form-horizontal', 'method'=>'POST'])}}

<div class="container-fluid">
	<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-2">
		<h2> Kies een drank om te verwijderen </h2>
		  {!! Form::Label('drink', 'Drink:') !!}
		  <select class="form-control" name="drink">
		    @foreach($drinks as $drink)
		      <option value="{{$drink->id}}">{{$drink->name}}</option>
		    @endforeach
		  </select>
		<button type="submit" class="btn btn-danger outline btn-block btn-lg" style="margin-top: 2em;" name="destroy">Verwijder drank</button>
		<a class="btn btn-warning outline btn-block last-button" style="margin-top: 1em;" href="{{route('admin')}}">Terug</a>
	</div>
</div>
{{Form::close()}}
@else
{{Form::open(['route'=>['drink.edit'], 'class'=>'form-horizontal', 'method'=>'GET'])}}

<div class="container-fluid">
	<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-2">
		<h2> Kies een drank om aan te passen </h2>
		  {!! Form::Label('drink', 'Drink:') !!}
		  <select class="form-control" name="drink">
		    @foreach($drinks as $drink)
		      <option value="{{$drink->id}}">{{$drink->name}}</option>
		    @endforeach
		  </select>
		<button class="btn btn-primary outline btn-block btn-lg" type="submit" style="margin-top: 2em;">Ga verder</button>
		<a class="btn btn-warning outline btn-block" style="margin-top: 1em;" href="{{route('admin')}}">Terug</a>
	</div>
</div>
{{Form::close()}}

@endif


@endsection

@extends('layouts.app')
@section('content')

{{Form::open(['route'=>['drink.store'], 'class'=>'form-horizontal', 'method'=>'POST'])}}

	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-xs-12">
				<label for="name"> Dranknaam </label>
				<input class="form-control" type="text" id="name" name="name">
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<label for="brand"> Merk </label>
				<input class="form-control" type="text" id="brand" name="brand">
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<label for="type"> Type </label>
				<select name="type" class="form-control">
					@foreach($types as $type)
						<option value="{{$type->name}}">{{$type->name}}</option>
					@endforeach
				</select>
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<label for="country"> Land van herkomst </label>
				<input class="form-control" type="text" id="country" name="country">
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<label for="region"> Regio </label>
				<input class="form-control" type="text" id="region" name="region">
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<label for="year"> Leeftijd </label>
				<input class="form-control" type="text" id="age" name="age">
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<label for="distillery"> Distilleerderij </label>
				<input class="form-control" type="text" id="distillery" name="distillery">
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<label for="characteristics"> Karakteristieken </label>
				<input class="form-control" type="text" id="characteristics" name="characteristics">
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<label for="barcode"> Barcode </label>
				<input class="form-control" type="text" id="barcode" name="barcode"/>
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<label for="price"> Prijs </label>
				<input class="form-control" type="text" id="price" name="price"/>
			</div>

		  	<div class="col-md-4 col-sm-6 col-xs-12">
		  		<label>Korting</label>
		  		<input type="checkbox" class="form-control" name="discount" id="discount"/>
		  	</div>

		  	<div class="col-md-4 col-sm-6 col-xs-12">
		  		<label>Kortingsbedrag</label>
		  		<input class="form-control" name="discount_price" id="discount_price"/>
		  	</div>

		  	<div class="col-md-4 col-sm-6 col-xs-12">
		  		<label>Mild tot Krachtig (1-12)</label>
		  		<input class="form-control" name="taste" id="taste"/>
		  	</div>

		  	<div class="col-md-4 col-sm-6 col-xs-12">
		  		<label>Zacht tot Rokerig</label>
		  		<input class="form-control" name="smokiness" id="smokiness"/>
		  	</div>

		  	<div class="col-md-4 col-sm-6 col-xs-12">
		  		<label>Vatsoort</label>
		  		<input class="form-control" name="cask" id="cask"/>
		  	</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label for="desciption"> Omschrijving </label>
				<textarea class="form-control" rows="10" id="description" name="description"></textarea>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
			  	<label>Korte Omschrijving</label>
			  	<textarea class="form-control" rows="10" name="description_short" id="description_short"></textarea>
		  	</div>


		</div>

		<button class="btn btn-primary outline btn-block btn-lg" type="submit" style="margin-top: 2em;"> Invoeren </button>
		<a class="btn btn-warning outline btn-block last-button" style="margin-top: 1em;" href="{{route('admin')}}">Terug</a>
	</div>

{{Form::close()}}
@endsection

@extends('layouts.app')

@section('content')
<div class="col-md-6 col-md-offset-3">
    <h2> Kies een drank om naar een tag te schrijven! </h2>
    <form class="form-horizontal" role="form" method="POST" action="{{ route('drink.writeToRfid') }}" >
        {{ csrf_field() }}
        <label for="id">Drink:</label>
        <select class="form-control" name="id">
            @foreach($drinks as $drink)
              <option value="{{$drink->id}}">{{$drink->name}}</option>
            @endforeach
        </select>
        <button class="btn btn-primary outline btn-block btn-lg" type="submit" style="margin-top: 2em;">Schrijf naar tag </button>
    </form>

    <a href="/admin" class="btn btn-warning outline btn-lg btn-block" style="margin-top:30px;">Terug</a>
</div>

@endsection

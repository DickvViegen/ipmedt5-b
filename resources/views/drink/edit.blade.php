@extends('layouts.app')
@section('content')

{{Form::open(['route'=>['drink.update'], 'class'=>'form-horizontal', 'method'=>'POST'])}}
<div class="container">
	<div class="row">
		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Name</label>
		  	<input class="form-control" name="name" value="{{$drink->name}}"/>
		  </div>

		  <div class="col-md-6 col-sm-6 col-xs-12 ">
		  	<label>Barcode</label>
		  	<input class="form-control" name="barcode" value="{{$drink->barcode}}"/>
		  </div>

		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Merk</label>
		  	<input class="form-control" name="brand" value="{{$drink->brand}}"/>
		  </div>

		  <div class="col-md-6 col-sm-6 col-xs-12">
				<label for="type"> Type </label>
				<select name="type" class="form-control">
					@foreach($types as $type)
						<option value="{{$type->name}}" {{ $selectedType == $type->name ? 'selected="selected"' : '' }}>{{$type->name}}</option>
					@endforeach
				</select>
			</div>

		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Leeftijd</label>
		  	<input class="form-control" name="age" value="{{$drink->age}}"/>
		  </div>

		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Distilleerderij</label>
		  	<input class="form-control" name="distillery" value="{{$drink->distillery}}"/>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Land</label>
		  	<input class="form-control" name="country" value="{{$drink->country}}"/>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Regio</label>
		  	<input class="form-control" name="region" value="{{$drink->region}}"/>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Prijs</label>
		  	<input class="form-control" name="price" value="{{$drink->price}}"/>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Korting</label>
		  	<input type="checkbox" class="form-control" name="discount" value="{{$drink->discount}}"/>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Kortingsprijs</label>
		  	<input class="form-control" name="discount_price" value="{{$drink->discount_price}}"/>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Mild tot krachtig (1-12)</label>
		  	<input class="form-control" name="taste" value="{{$drink->taste}}"/>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Zacht tot rokerig (1-12)</label>
		  	<input class="form-control" name="smokiness" value="{{$drink->smokiness}}"/>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Vatsoort</label>
		  	<input class="form-control" name="cask" value="{{$drink->cask}}"/>
		  </div>


		  <div class="col-md-6 col-sm-6 col-xs-12">
		  	<label>Karakteristieken</label>
		  	<input class="form-control" name="characteristic" value="{{$drink->characteristic}}"/>
		  </div>

		  <div class="col-md-12 col-sm-12 col-xs-12">
		  	<label>Omschrijving</label>
		  	<textarea class="form-control" name="description" rows="10" value="">{{$drink->description}}</textarea>
		  </div>
		<div class="col-md-12 col-sm-12 col-xs-12">
		  	<label>Korte Omschrijving</label>
		  	<textarea class="form-control" name="description_short" rows="10" value="">{{$drink->description_short}}</textarea>
		</div>

		</div>
		<input type="hidden" name="id" value="{{$drink->id}}">
		<button class="btn btn-primary outline btn-block btn-lg" type="submit" style="margin-top: 2em;"> Pas aan </button>
		<a class="btn btn-warning outline btn-block last-button" style="margin-top: 1em;" href="{{route('admin')}}">Terug naar dashboard</a>
</div>
{{Form::close()}}
@endsection

@extends('layouts.app')

@section('content')

@if(Auth::user())
<div class="col-md-6 col-md-offset-3">
    <a href="/admin" class="btn btn-large outline btn-primary btn-block">Naar het dashboard</a>
</div>
@else
<div class="col-md-6 col-md-offset-3">
    <a href="login" class="btn btn-large outline btn-primary btn-block">Log in</a>
</div>
@endif



@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

// Alle admin dashboard routes moeten over de authenticatie middleware
Route::group(['prefix' => '/admin', 'middleware' => 'auth'], function() {
	Route::get('/', 'AdminController@index')->name('admin');

	// Drink routes
	Route::group(['prefix' => '/drinks'], function() {
		Route::get('/', 'DrinkController@create')->name('drink.create');
		Route::post('/', 'DrinkController@store')->name('drink.store');
		Route::get('/select', 'DrinkController@select')->name('drink.select');
		Route::get('/edit', 'DrinkController@edit')->name('drink.edit');
		Route::post('/edit', 'DrinkController@update')->name('drink.update');
        Route::get('/delete','DrinkController@select')->name('drink.selectDestroy');
        Route::post('/delete', 'DrinkController@destroy')->name('drink.destroy');

		Route::get('/write','DrinkController@write')->name('drink.write');
		Route::post('/write','DrinkController@writeToRfid')->name('drink.writeToRfid');
	});

	// Type routes
	Route::group(['prefix' => '/type'], function() {
		Route::get('/', 'TypeController@create')->name('type.create');
		Route::post('/', 'TypeController@store')->name('type.store');
		Route::get('/edit', 'TypeController@edit')->name('type.edit');
		Route::post('/edit', 'TypeController@update')->name('type.update');
		Route::get('/select', 'TypeController@select')->name('type.select');
		Route::post('/delete', 'TypeController@destroy')->name('type.destroy');
	});
});

// Authenticatie routes
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Alle API requests moeten door de whitelist, en een cors-header meekrijgen
Route::group(['middleware' => 'whitelist:reactapp'], function() {
  Route::get('/product/{id}','DrinkController@show')->middleware('cors');
  Route::get('/','DrinkController@index')->middleware('cors');
  Route::post('/product/{id}','DrinkController@updateViews')->middleware('cors');
});

// De endpoint voor een gebruiker die niet op de whitelist staat.
Route::get('/noaccess', function () {
    return view('api.noaccess');
});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDrinkTable extends Migration {

	public function up()
	{
		Schema::create('drink', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->bigInteger('barcode')->nullable();
			$table->integer('type_id')->unsigned();
			$table->string('brand');
			$table->string('volume')->default('70cl');
			$table->string('age')->nullable();
			$table->string('distillery')->nullable();
			$table->string('country');
			$table->string('region');
			$table->string('price');
			$table->longText('description');
			$table->longText('description_short');
			$table->integer('taste');
			$table->integer('smokiness');
			$table->string('cask')->nullable();
			$table->boolean('discount')->default(false);
			$table->string('discount_price')->nullable();
			$table->string('path_to_img')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('drink');
	}
}

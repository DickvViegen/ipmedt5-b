<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    //  Een test user voor de demo case
    public function run()
    {
        $user = new User;

        $user->name = "Admin";
        $user->email = "info@winesandmore.nu";
        $user->password = "test";
        $user->save();
    }
}

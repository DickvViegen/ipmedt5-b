<?php

use Illuminate\Database\Seeder;
use App\Drink;
use Carbon\Carbon;

class DrinkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    //  Verschillende inserts, omdat we niet allemaal dezelfde kolommen vullen per drank.
    public function run()
    {
        Drink::insert([
            [
                'name' =>  'Clynelish 1996 Un-Chillfiltered',
                'type_id'=>  1,
                'brand' => 'Signatory Vintage',
                'age' =>	'20',
	            'country' => 'Schotland',
	            'region' => 'Highlands',
                'price' => '66,99',
                'description' => "Twintig jaar oude heerlijke whisky, rijk en intens met een breed pallet aan karakteristieke smaken. De Clynelish Single Malt is een single malt whisky, geproduceerd in de Clynelish Distillery in Brora, Sutherland in de Schotse Hooglanden. In de Clynelish distilleerderij staan drie wash en drie spirit stills, die met stoom gestookt worden. De washbacks zijn van larikshout. Voor de rijping van de new spirit worden bourbon, sherry en refill vaten gebruikt. Men haalt het water voor het koelen als ook het water voor het brouwen, via een pijpleiding, uit het riviertje de Clynemilton Burn. Dit riviertje stroomt net ten noorden van de distilleerderij de Noordzee in. De Clynelish malt wordt hoofdzakelijk gebruikt voor de blended whisky's.",
                'description_short' => 'Twintig jaar oude heerlijke whisky, rijk en intens met een breed pallet aan karakteristieke smaken.',
                'taste' => 8,
                'smokiness' => 9,
                'cask' => 'Refill butt',
                'path_to_img' => 'http://i.imgur.com/5M2gxH7.jpg',
            ],
            [
                'name' =>  'The Ultimate Longmorn',
                'type_id'=>  1,
                'brand' => 'Longmorn',
                'age' =>	'25',
	            'country' => 'Schotland',
	            'region' => 'Speyside',
                'price' => '95,99',
                'description' =>"Deze mooie, oude Longmorn werd door van Wees geselecteerd voor het Ultimate label. De whisky werd gedistilleerd op 15 juni 1990 en rijpte 25 jaar op hogshead #8578. De naam Longmorn is afkomstig van de boerderij met dezelfde naam, waar men is begonnen met het distilleren van whisky. Longmorn betekent vertaald uit het Engels lange morgen, maar in het Gaelic is de betekenis van de naam Heilige Bron. De distilleerderij is opgericht in 1894 door John Duff met zijn twee partners. Door het stevige Speyside karakter van de malt van Longmorn is deze zeer geliefd bij blenders. De malt van de Longmorn Distilleerderij wordt wel Speyside’s best kept secret genoemd. Longmorn is nu in het bezit van de Chivas & Glenlivet Group, die onderdeel zijn van de Pernod/Ricard Groep. Een groot deel van het succes van Longmorn is de ligging aan de rand van het belangrijkste gebied voor graan, n.l. de Larch o’Moray, dat bekend staat om zijn micro klimaat en vruchtbare grond.",
                'description_short' => 'Deze mooie, oude Longmorn werd door van Wees geselecteerd voor het Ultimate label.',
                'taste' => 5,
                'smokiness' => 7,
                'cask' => 'Okshoofd',
                'path_to_img' => 'http://i.imgur.com/0lG0Z5k.jpg',
            ],
            [
                'name' =>  'Caol Ila',
                'type_id'=>  1,
                'brand' => 'Caol Ila',
                'age' =>	'12',
	            'country' => 'Schotland',
	            'region' => 'Highlands',
                'price' => '48,99',
                'description' => "Caol Ila 12 years kenmerkt zich door zijn krachtige, stoere karakter en het medium gebruik van turf. In geur en smaak komt duidelijk het complexe rokerige karakter naar voren. Caol Ila, uitgesproken als 'kool iela', komt uit het Gaelic en betekent 'Sound of Islay'. Het is de naam van het water tussen de eilanden Jura en Islay. In de distilleerderij staan nu drie wash stills en drie spirit stills, die met stoom worden verwarmd. De acht washbacks zijn van larikshout. Het rijpen van de new spirit gebeurt bijna uitsluitend op refill vaten. Er liggen ook wel nieuwe sherryvaten in het warehouse. Het brouwwater komt uit een meertje Loch nam Ban geheten. Het meertje wordt gevoed door regenwater en gaat via een pijpleiding naar de distilleerderij.",
                'description_short' => 'Caol Ila 12 years kenmerkt zich door zijn krachtige, stoere karakter en het medium gebruik van turf. In geur en smaak komt duidelijk het complexe rokerige karakter naar voren.',
                'taste' => 10,
                'smokiness' => 10,
                'cask' => 'Eiken',
                'path_to_img' => 'http://i.imgur.com/rX5FuKv.jpg',
            ],
            [
                'name' =>  'Lagavulin',
                'type_id'=>  1,
                'brand' => 'Lagavulin',
                'age' =>	'16',
	            'country' => 'Schotland',
	            'region' => 'Islay',
                'price' => '64,99',
                'description' => "De Lagavulin distilleerderij ligt op de zuidelijke kust van het beroemde whiskyproducerende eiland Islay. Het kasteel Dunyveg bewaakt de doorgang naar Lagavulin Bay. Dit was de vesting van de 'Lords of the Isles', die eeuwen geleden regeerde over de zeeën langs de westkust van Schotland, en die vreemdelingen buiten hielden. Tegenwoordig is het kasteel een prachtige ruïne, en zijn de eilanders veel gastvrijer. Bezoekers uit alle hoeken van de wereld komen op bedevaartstocht naar de distilleerderij en de zeven anderen op dit eiland die nog in werking zijn. De naam Lagavulin komt uit het Gaelic van 'Laggan Mhouillin' en betekent 'Het ondiepe dal waar de molen staat'.  Lagavulin is met zijn 16 jaar een grootse malt van Islay en heeft alles in zich wat dit eiland zo beroemd heeft gemaakt: turf, zeewier, hout, fruit en bovenal complexiteit. Wellicht het beste wat Schotland te bieden heeft.",
                'description_short' => ' Lagavulin is met zijn 16 jaar een grootse malt van Islay en heeft alles in zich wat dit eiland zo beroemd heeft gemaakt: turf, zeewier, hout, fruit en bovenal complexiteit.',
                'taste' => 10,
                'smokiness' => 11,
                'cask' => 'Larikshout, Bourbon',
                'path_to_img' => 'http://i.imgur.com/bUzKodY.jpg',
            ],
        ]);

        Drink::insert([
            [
                'name' =>  'Tomatin Legacy',
                'type_id'=>  1,
                'brand' => 'Tomatin',
                'age' => 'no-age',
	            'country' => 'Schotland',
	            'region' => 'Highlands',
                'price' => '28,99',
                'description' => "Legacy is de entree-whisky van Tomatin. Het gebruik van bourbon- en virgin oak vaten maakt hem tot een heel aangename, friszoete dram. Tomatin ligt op zo'n 315 meter boven de zeespiegel en is de op drie na hoogst gelegen distilleerderij van Schotland. De naam Tomatin is afkomstig van de gelijknamige plaats in de buurt en wordt uitgesproken als 'Tomaat-in'. Tomatin komt uit het Gaelic en betekent 'de heuvel van de jeneverbosjes'. Het water dat men gebruikt voor de productie van whisky komt uit de Alt-na-Frith Burn. Deze Burn wordt gevoed door regen en sneeuw afkomstig van de Monadhlaith bergen en mondt uit in de Findhorn rivier, van waaruit het koelwater voor de condensors wordt gehaald. Slechts een kleine hoeveelheid Tomatin wordt als single maltwhisky op de markt gebracht, het grootste deel wordt voor de blended whisky's gebruikt.",
                'description_short' => 'Legacy is de entree-whisky van Tomatin. Het gebruik van bourbon- en virgin oak vaten maakt hem tot een heel aangename, friszoete dram.',
                'taste' => 4,
                'smokiness' => 8,
                'cask' => 'Bourbon- en Virgin Eiken',
                'discount' => true,
                'discount_price' => '24,99',
                'path_to_img' => 'http://i.imgur.com/8q2EYI1.jpg',
            ],
        ]);

        Drink::insert([
            [
                'name' =>  'Glenfarclas',
                'type_id'=>  1,
                'brand' => 'Glenfarclas',
                'age' => '12',
                'volume' => '1 liter',
	            'country' => 'Schotland',
	            'region' => 'Speyside',
                'price' => '44,99',
                'description' => "Glenfarclas kreeg in 1836 een vergunning. De distilleerderij werd gebouwd aan de voet van de berg Ben Rinnes in Speyside, bij de Rechlerich boerderij. De merknaam Glenfarclas is afkomstig uit het Gaelisch en is letterlijk vertaald 'Vallei van het groene grasland'. In 1865 werd de distilleerderij door John Grant overgenomen. Ze werd al snel een favoriete ontmoetingsplaats voor passerende boeren en marktlui. Glenfarclas is nog steeds eigendom van de familie Grant en nu staat de vijfde generatie Grant aan het roer van de distilleerderij. De drie wash stills en de drie spirit stills worden niet met stoom verwarmd maar met gasvlammen onder de stills. Dit is voor de distilleerderijen in Schotland zeer ongewoon. De twaalf wash backs zijn gemaakt van roestvrij staal en de new spirit rijpt op nieuwe sherryvaten, maar men gebruikt ook refill sherryvaten. Het water dat gebruikt wordt voor de bereiding van de whisky en het koelwater komt via beekjes van de Ben Rinnes. ",
                'description_short' => 'Glenfarclas 12 years is een whisky met veel zoete tonen.',
                'taste' => 7,
                'smokiness' => 3,
                'cask' => 'Sherry',
                'discount' => true,
                'discount_price' => '37,99',
                'path_to_img' => 'http://i.imgur.com/9jIV0oG.jpg',
            ],
        ]);

        Drink::insert([
            [
                'name' =>  'Jura Superstition',
                'type_id'=>  1,
                'brand' => 'Isle of Jura',
                'age' => 'mixed',
                'volume' => '1 liter',
	            'country' => 'Schotland',
	            'region' => 'Jura',
                'price' => '51,99',
                'description' => "Superstition is een vernieuwende whisky, een creatie van het samengaan van de traditionele Islay stijl, namelijk malt van het graan met de bekende turfsmaak en die van een selectie van oudere maltwhisky's, een verbond tussen de whiskystijlen van het eiland Jura, een balans tussen turf en zoeter graan. De een krachtig en sterk van smaak met turf, de ander warm en verfijnd. De naam Superstition stamt uit het geloof van de bevolking van de Hebriden, de Schotse eilandengroep van mythen en legendes. In het stillhouse van de distilleerderij Isle of Jura staan twee wash stills en twee spirit stills. Deze nogal tamelijk grote stills worden met stoom verhit. De zes washbacks zijn van roestvrij staal en voor de rijping gebruikt men refill vaten. Het water haalt men via een pijpleiding naar de distilleerderij en is tamelijk turfhoudend. De lichte turfsmaak van de malt van Isle of Jura is dan ook afkomstig van het water dat men gebruikt, aangezien de mout niet met turf wordt gedroogd.",
                'description_short' => 'Individueel! Mysterieus! Sensationeel! Deze karaktereigenschappen vatten de interessante Superstition maltwhisky Isle of Jura samen. ',
                'taste' => 7,
                'smokiness' => 7,
                'cask' => 'Refill butt',
                'discount' => true,
                'discount_price' => '47,99',
                'path_to_img' => 'http://i.imgur.com/R86UhiY.jpg',
            ],
        ]);

        Drink::insert([
            [
                'name' =>  'Talisker Storm',
                'type_id'=>  1,
                'brand' => 'Talisker',
                'age' => 'no-age',
	            'country' => 'Schotland',
	            'region' => 'Skye',
                'price' => '47,99',
                'description' => "Talisker is de enige malt whisky die geproduceerd wordt op het eiland Skye. Hij vindt zijn oorsprong in 1830, de unieke opstelling van de ketels bezorgt Talisker zijn speciale karakter. De naam komt van het Noors “Thalas Gair” wat “hellende rots” betekent.  Talisker heeft een traditionele wijze voor het produceren van haar whisky. De typische geur en smaak is ontstaan door het mout dat met behulp van turf voorzien word van een relatief hoog Fenolgehalte. Ook het gebruikte water uit Cnon Nan Speireag wordt over het turf geleid. Talisker staat ook om het pittige karakter en de wat zilte smaak bekend.",
                'description_short' => 'Talisker Storm nieuwe uiting van Talisker, intenser en rokeriger met diepere en krachtige ziltige ondertonen die in balans zijn met de kenmerkende warme zoetheid van Talisker.',
                'taste' => 11,
                'smokiness' => 11,
                'cask' => 'Refil butt',
                'path_to_img' => 'http://i.imgur.com/VSbDrH5.jpg',
            ],
        ]);

        Drink::insert([
            [
                'name' =>  'Ileach Peated Islay Malt',
                'type_id'=>  1,
                'brand' => 'Higlands and Islands Whisky Company',
                'age' => '?',
                'volume' => '1 liter',
	            'country' => 'Schotland',
	            'region' => 'Islay',
                'price' => '31,99',
                'description' => 'Deze peated single malt komt van het eiland Islay en wordt ook wel "The Man from Islay" genoemd. Die verwijzing is ontstaan doordat het een robuuste en turfige karakter heeft. Daarnaast is het de vraag van welke distilleerderij deze single malt whisky afkomstig is want dat is onbekend en wordt niet aangegeven op de verpakking. Ongeacht van welke geheime distilleerderij deze dram vandaan komt, een topper is het zeker!',
                'description_short' => '“The Man from Islay”. Een jonge Islay single malt. Er zijn maar weinig mensen die weten van welke distilleerderij deze whisky afkomstig is.',
                'taste' => 10,
                'smokiness' => 10,
                'discount' => true,
                'discount_price' => '27,49',
                'path_to_img' => 'http://i.imgur.com/ajMbmO3.jpg',
            ],
        ]);

        Drink::insert([
            [
                'name' =>  'Tomatin 14 Port Casks',
                'type_id'=>  1,
                'brand' => 'Tomatin',
                'age' =>	'14',
	            'country' => 'Schotland',
	            'region' => 'Highlands',
                'price' => '54,99',
                'description' => "Prachtige 14 jaar oude Tomatin met een unieke zeer volle rijping op Port Casks. Tomatin ligt op zo'n 315 meter boven de zeespiegel en is de op drie na hoogst gelegen distilleerderij van Schotland. De naam Tomatin is afkomstig van de gelijknamige plaats in de buurt en wordt uitgesproken als 'Tomaat-in'. Tomatin komt uit het Gaelic en betekent 'de heuvel van de jeneverbosjes' . Het water dat men gebruikt voor de productie van whisky komt uit de Alt-na-Frith Burn. Deze Burn wordt gevoed door regen en sneeuw afkomstig van de Monadhlaith bergen en mondt uit in de Findhorn rivier, van waaruit het koelwater voor de condensors wordt gehaald. Slechts een kleine hoeveelheid Tomatin wordt als single maltwhisky op de markt gebracht, het grootste deel wordt voor de blended whisky's gebruikt.",
                'description_short' => 'Prachtige 14 jaar oude Tomatin met een unieke zeer volle rijping op Port Casks. Door deze mooie rijping is de whisky goud bruin van kleur met heerlijke smaken van noot, walnoot, abrikoos en veel fruit.',
                'taste' => 8,
                'smokiness' => 7,
                'cask' => 'Bourbon, Port',
                'path_to_img' => 'http://i.imgur.com/zHaoUiW.jpg',
            ],
        ]);
    }
}

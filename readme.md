# IPMEDT5 - Groep B

## Internet of Things - LoRaWAN

Een project voor de Hogeschool Leiden en voor retailers in Leiden en omstreken.

## Installatie instructies project
Zorg er eerst voor dat [Git](https://git-scm.com/) geïnstalleerd is op je machine, en je public key toegevoegd is aan je [BitBucket](https://bitbucket.org)-account. Als je nog geen keypair hebt, genereer je deze volgens de [guide van BitBucket](https://confluence.atlassian.com/bitbucket/how-to-install-a-public-key-on-your-bitbucket-cloud-account-276628835.html).

1. [Installeer Vagrant](https://www.vagrantup.com/) en [Virtualbox](https://www.virtualbox.org/) (of [Parallels](https://www.parallels.com/eu/landingpage/pd/general/?gclid=CITOnNOVl9ICFYwQ0wodPV0Hew)(OSX) of [VMWare](http://www.vmware.com/)) op je eigen machine.
2. Op Windows, open *Git bash*. Voor UNIX-systemen: open je terminal editor.
3. Clone de repository: `git clone git@bitbucket.org:DickvViegen/ipmedt5-b.git`
4. CD naar het project `cd ipmedt5-b`
5. Installeer homestead
    * Mac/Linux `php vendor/bin/homestead make`
    * Windows `vendor\bin\homestead make`
6. `cp .env.example .env`
7. `php artisan key:generate`
8. Draai vanuit deze map `vagrant up`
9. Voeg de volgende regel toe aan je hosts file: `192.168.10.10 ipmedt5.app`
    * Via dit domein is de web app te bereiken
    * Op UNIX-systemen is de hosts file: `/etc/hosts`
    * Op Windows-systemen is de hosts file: `%SystemRoot%\System32\drivers\etc\hosts`
10. Instructies voor de database zullen onder het kopje [MySQL](#MySQL) staan.


## MySQL




![Laravel](https://laravel.com/assets/img/components/logo-laravel.svg)
[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)


## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).